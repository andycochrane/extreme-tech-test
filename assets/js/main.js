window.addEventListener('load', function() {

  $('.js-toggle-dropdown').on('click', function() {
    $(this).toggleClass('is-active').next().toggleClass('is-active');
  });

  $('.js-toggle-nav--open').on('click', function() {
    $(this).addClass('is-inactive');
    $('.js-toggle-nav--close').removeClass('is-inactive');
    $('.js-nav-primary').addClass('is-active');
  });

  $('.js-toggle-nav--close').on('click', function() {
    $(this).addClass('is-inactive');
    $('.js-toggle-nav--open').removeClass('is-inactive');
    $('.js-nav-primary').removeClass('is-active');
  });
});
