'use strict';

const gulp = require('gulp')
const uglify = require('gulp-uglify')
const sass = require('gulp-sass')

const paths = {
  js: './assets/js/**/*.js',
  sass: './assets/sass/**/*.scss'
};


gulp.task('js', function () {
  return gulp.src('./assets/js/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./js'));
});

gulp.task('sass', function () {
  return gulp.src('./assets/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('watch', function () {
  gulp.watch(paths.js, ['js']);
  gulp.watch(paths.sass, ['sass']);
});
